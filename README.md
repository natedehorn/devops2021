# DevOps2021

Run `docker-compose up` to run the docker-compose file.

## GitLab

When the container is running, navigate to port 8000 from a browser. To login, use username: `root` and use the password from the ./config/gitlab/initial_root_password file.

### Container Registry

To log into the container registry, type the following into the command line of the host machine: `docker login localhost:4567 -u <GitLab username>`. You will then be prompted for your GitLab password.

To push an image, take the following steps:
1. Tag the image - `docker tag <image name> </path/to/registry/tag`
2. Push the image - `docker push <new image name>`

To create a Jenkins webhook, take the following steps:
1. Navigate to the admin panel. Undder Settings > Network > Outbound requests, check the box allowing webhooks to local sites.
2. Navigate to Settings > Webhooks in the GitLab project
3. Add the following url - http://admin:[jenkins api token]@jenkins:8080/projects/gitlab-trigger
4. Check the push events box and disable SSL verification 
5. Don't bother testing the webhook as that functionaly seems to be broken. Test the webhook using actual git commits.

## SonarQube

When the container is running, navigate to port 9000 from a browser. To login, use username: `admin` and password: `admin`. You will be prompted to change the password upon initial login.

SonarQube requires a database to store its findings. A postgres db service has been added to the docker compose file and will stand up a postgres instance on port 5432.

#### Startup Issues

You may encounter an issue when sonarqube is starting up related to elastic search, stating `max virtual memory areas vm.max_map_count is too low`. To address this, increase the `vm.max_map_count` value of docker. 

On Windows:
```
1) wsl -d docker-desktop
2) sysctl -w vm.max_map_count=262144
```

## GitLab Runner

To start the container with GitLab runner, you must configure the runner to work with the instance of docker you have set up. To do this, start the container with `docker-compose up -d` then configure the runner by running `docker-compose exec -T gitlab-runner gitlab-runner register`